package com.firstherokuapp.dto;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class StringUtils {
	
	public static String convertNumberToString(int number, int len) {
		String numberStr = String.valueOf(number);
		int remain = len - numberStr.length();
		
		while(remain > 0) {
			numberStr = "0" + numberStr;
			remain--;
		}
		return numberStr;
	}
	
	public static void baseWriteTicketToFile(String filePath, ArrayList<int[]> ticketList) {
		try {
			if(ticketList == null || ticketList.isEmpty()) {
				return;
			}
			File file = new File(filePath);
			if(!file.exists()) {
				file.createNewFile();
			}
			FileWriter writer = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bufferWriter = new BufferedWriter(writer);
			for(int[] key : ticketList) {
				for(int i = 0; i < key.length - 1; i++) {
					bufferWriter.write(key[i] + " ");
				}
				bufferWriter.write(key[key.length - 1] + "\n");
			}
			bufferWriter.close();
			writer.close();
			System.out.println("Done");
		} catch(IOException e) {
			e.printStackTrace();
			return;
		}
	}
	
	public static int[] convertArrayListToInt(ArrayList<Integer> a) {
    	int[] primitiveList = new int[a.size()];
    	for(int i = 0; i < a.size(); i++) {
    		primitiveList[i] = a.get(i).intValue();
    	}
    	Arrays.sort(primitiveList);
    	return primitiveList;
    }
	
	public static ArrayList<Integer> copyArray(ArrayList<Integer> originList) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		if(originList == null || originList.size() == 0) {
			return result;
		}
		for(Integer i : originList) {
			result.add(i.intValue());
		}
		return result;
	}
}
