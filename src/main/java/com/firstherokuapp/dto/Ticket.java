package com.firstherokuapp.dto;

import java.util.ArrayList;

public class Ticket {
	private String id;
	private ArrayList<Integer> code;
	
	public Ticket() {
		id = "";
		code = new ArrayList<Integer>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ArrayList<Integer> getCode() {
		return code;
	}

	public void setCode(ArrayList<Integer> code) {
		this.code = code;
	}
	
	
}
