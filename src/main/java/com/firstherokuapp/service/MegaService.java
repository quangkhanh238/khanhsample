package com.firstherokuapp.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.firstherokuapp.Utils.SSLHelper;
import com.firstherokuapp.dto.StringUtils;
import com.firstherokuapp.dto.Ticket;
import com.firstherokuapp.model.Mega;
import com.firstherokuapp.repository.MegaRepository;

@Service
public class MegaService {
	
	private final String MEGA_URL = "https://vietlott.vn/vi/trung-thuong/ket-qua-trung-thuong/645";
	private final String CHITIETKETQUA = "chitietketqua_title";
	private final String CLASS_RESULT = "day_so_ket_qua_v2";

	@Autowired
	private MegaRepository repo;
	
	private int getLastestNumbers() {
		try {
			Document doc = SSLHelper.getConnection(MEGA_URL).get();
			Element strLastestTerm = doc.selectFirst("div.chitietketqua_title h5 b");
			String latestTerm = strLastestTerm.text().replace("#", "");
			int term = Integer.valueOf(latestTerm);
			return term;
		} catch (IOException e) {
			
		}
		return 1;
	}
	
	@Transactional(rollbackFor = Exception.class)
	private void saveTicketFromServerToDatabase(int from, int to) {
		try {
			for(int i = from; i <= to; i++) {
				String id = StringUtils.convertNumberToString(i, 5);
				Document doc = SSLHelper.getConnection(MEGA_URL + "?id=" + id + "&nocatche=1").get();
				Elements listResult = doc.getElementsByClass(CLASS_RESULT);
				Element span = listResult.get(0);
				Elements numbers = span.children();
				StringBuilder builder = new StringBuilder();
				for(int j = 0; j < numbers.size(); j++) {
					builder.append(numbers.get(j).text()).append(" ");
				}
				String ticket = builder.toString().trim();
				Mega mega = new Mega();
				mega.setId((long) i);
				mega.setNumbers(ticket);
				repo.save(mega);
				System.out.println("Stored: " + id);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private List<Ticket> loadAllTicket() {
		List<Ticket> lstTicket = new ArrayList<>();
		Iterable<Mega> lstMega = repo.findAll();
		Iterator<Mega> iterator = lstMega.iterator();
		Mega mega = null;
		while(iterator.hasNext()) {
			mega = iterator.next();
			Ticket ticket = new Ticket();
			ticket.setId(StringUtils.convertNumberToString(mega.getId().intValue(), 5));
			String[] numbersRaw = mega.getNumbers().split(" ");
			for(int i = 0; i < numbersRaw.length; i++) {
				ticket.getCode().add(Integer.valueOf(numbersRaw[i]));
			}
			lstTicket.add(ticket);
		}
		return lstTicket;
	}
	
	private Map<Integer, Double> bestPickAtPosition(List<Ticket> lstTicket, int position, int min) {
		position = position - 1;
		HashMap<Integer, Integer> freq = new HashMap<>();
		HashMap<Integer, Double> freq2 = new HashMap<>();
		for(int i = 0; i < lstTicket.size(); i++) {
			Integer numAtPos = lstTicket.get(i).getCode().get(position);
			if(freq.containsKey(numAtPos)) {
				freq.put(numAtPos, freq.get(numAtPos).intValue() + 1);
			} else {
				freq.put(numAtPos, 1);
			}
		}
		for(Map.Entry<Integer, Integer> entry : freq.entrySet()) {
			if(entry.getKey() <= min) {
				continue;
			}
			Double d = (((double) entry.getValue()) / lstTicket.size()) * 100;
			d = Math.round(d * 100) / 100D;
			freq2.put(entry.getKey(), d);
		}
		return freq2;
	}
	/*
	public Map.Entry<Integer, Double> find6Numbers(List<Ticket> lstTicket, Map.Entry<Integer, Double> entry, int position) {
		int min = 0;
		if(entry != null) {
			min = entry.getKey();
		}
		
		Map<Integer, Double> lstPos = bestPickAtPosition(lstTicket, position, min);
		for(Map.Entry<Integer, Double> subEntry : lstPos.entrySet()) {
			if(position == 6) {
				return subEntry;
			}
			find6Numbers(lstTicket, subEntry, position + 1);
		}
	}
	*/
	public List<Ticket> predictNumbers() {
		int lastestTerm = getLastestNumbers();
		int storedTerm = 1;
		Mega storedMega = repo.findTop1ByOrderByIdDesc();
		if(storedMega != null) {
			storedTerm = storedMega.getId().intValue();
		}
		if(lastestTerm > storedTerm) {
			saveTicketFromServerToDatabase(storedTerm + 1, lastestTerm);
		}
		//load all ticket
		List<Ticket> lstTicket = loadAllTicket();
		Map<Integer, Double> lstPos = bestPickAtPosition(lstTicket, 6, 34);
		System.out.println(lstPos.toString());
		return null;
	}
}
