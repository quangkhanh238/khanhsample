package com.firstherokuapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.firstherokuapp.model.Mega;

public interface MegaRepository extends PagingAndSortingRepository<Mega, Long>{

	Mega findTop1ByOrderByIdDesc();
}
