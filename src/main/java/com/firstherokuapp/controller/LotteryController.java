package com.firstherokuapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.firstherokuapp.dto.Ticket;
import com.firstherokuapp.service.MegaService;

@RestController
@RequestMapping(value = "/service")
public class LotteryController {

	@Autowired
	private MegaService megaService;
	
	@RequestMapping("khanhhello")
	public ResponseEntity<?> testHello() {
		String test = new String("ahihi");
		return new ResponseEntity<String>(test, HttpStatus.OK);
	}
	
	@RequestMapping("predictMega")
	public ResponseEntity<?> predictMega() {
		List<Ticket> lstTicket = megaService.predictNumbers();
		return new ResponseEntity(lstTicket, HttpStatus.OK);
	}
}
