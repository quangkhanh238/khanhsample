package com.firstherokuapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableWebMvc
public class KhanhlotteryApplication {

	public static void main(String[] args) {
		System.setProperty("lottery", "web-server");
		ApplicationContext ctx = SpringApplication.run(KhanhlotteryApplication.class, args);
		DispatcherServlet dispatcher = (DispatcherServlet) ctx.getBean("dispatcherServlet");
		dispatcher.setThrowExceptionIfNoHandlerFound(true);
	}

}
